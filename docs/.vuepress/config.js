module.exports = {
    title: 'GitLab ❤️ Thanakrit',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/thanakrit/',
    dest: 'public'
}